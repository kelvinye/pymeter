#!/usr/bin python3
# @File    : transaction.py
# @Time    : 2021/9/29 16:20
# @Author  : Kelvin.Ye
import httpx
from loguru import logger

from pymeter.configs.arguments import Arguments
from pymeter.configs.httpconfigs import SessionManager
from pymeter.engines.interface import NoConfigMerge
from pymeter.engines.interface import TransactionConfig
from pymeter.engines.interface import TransactionListener
from pymeter.workers.context import ContextService


class TransactionParameter(Arguments, TransactionConfig, NoConfigMerge, TransactionListener):

    def transaction_started(self) -> None:
        """@override"""
        variables = ContextService.get_context().variables
        for key, value in self.to_dict().items():
            if not key:
                continue
            variables.put(key, value)

    def transaction_ended(self) -> None:
        """@override"""
        ...


class TransactionHTTPSessionManager(SessionManager, TransactionConfig, TransactionListener):

    def transaction_started(self) -> None:
        """@override"""
        logger.debug('创建事务 HTTP 会话')
        self.session = httpx.Client()

    def transaction_ended(self) -> None:
        """@override"""
        logger.debug('关闭事务 HTTP 会话')
        self.session.close()
