#!/usr/bin python3
# @File    : python_code_snippets.py
# @Time    : 2021/11/3 13:06
# @Author  : Kelvin.Ye


INDENT = '    '


DEFAULT_IMPORT_MODULE = '''
import random
from pymeter.utils.json_util import to_json
from pymeter.utils.json_util import from_json

'''


DEFAULT_LOCAL_IMPORT_MODULE = '''
    import random
    from pymeter.utils.json_util import to_json
    from pymeter.utils.json_util import from_json

'''
